# habi-devops-test

## Context

Hello Habi team! This is my repository of my technical test, it consists of a solution architecture to deploy a lambda through IaC in this case CloudoFormation, additionally the CI/CD cycle is done through GitLabCi where the code can be updated and each time it is perform a push the integration and deployment pipeline is executed

## Architecture

<img src="diagr.png" height="400" alt="Screenshot"/>


This architecture is based on an AWS environment where a lambda is consumed through ApiGateway because the functionality of the lambda can be exposed to a REST client.

## How to use

It is an API in python which allows sending POST type requests with three input parameters which are:

-transactionId(String)

-type(String)

-amount(String)

The lambda will validate this data and convert it to deliver it in the response with an additional message and a status of 200.

Api url -> https://y2xpjod66j.execute-api.us-east-1.amazonaws.com/prod/lambda/
Method -> POST

## Thanks ! :) 


